package com.example.dima.codalineleson2;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Admin on 11.02.2016.
 */
public class ActivityFragment2 extends Fragment implements InterfaceFragment {
    static TextView textView = null;
    static String line = "";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_fragment2, null);
        return view;
    }

    @Override
    public void onStart() {
        textView = (TextView) getActivity().findViewById(R.id.fragment2_text_view);
        line = getString(R.string.label_count);
        super.onStart();
    }

    @Override
    public void setCountChar(int length) {

        textView.setText(line+" = "+String.valueOf(length));
    }
}
