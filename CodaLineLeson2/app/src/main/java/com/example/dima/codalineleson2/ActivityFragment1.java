package com.example.dima.codalineleson2;

import android.os.Bundle;
import android.renderscript.Int2;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Admin on 11.02.2016.
 */
public class ActivityFragment1 extends Fragment  {

    EditText editText = null;
    InterfaceFragment interfaceFragment = null;

    @Override
    public void onStart() {
        super.onStart();
        interfaceFragment = new ActivityFragment2();
        editText = (EditText) getActivity().findViewById(R.id.fragment1_edit_text);
        editText.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                interfaceFragment.setCountChar(editText.getText().toString().length());
            }
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            final View view = inflater.inflate(R.layout.activity_fragment1,null);
            return view;
        }
    }
